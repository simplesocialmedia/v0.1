# SimpleSocialMedia(aka Circle)

The SimpleSocialMedia project was an attempt of two friends to implement a social media on a basic level, (using phpMyAdmin, Xamarin.Android and Python3.0)
as part of our final project in the Magshimim Cyber Program(Israel), 2019. 

## Installation of the code
#### Client
You'll need to have Xamarin.Android and .NET features installed on visual studio.
#### Server
You'll need to install the following libs for the python server:

```bash
pip install mysql-connector 
```

```bash
pip install passlib
```
#### Database
You'll need to install XAMPP:([https://www.apachefriends.org/xampp-files/7.3.4/xampp-windows-x64-7.3.4-0-VC15-installer.exe](https://www.apachefriends.org/xampp-files/7.3.4/xampp-windows-x64-7.3.4-0-VC15-installer.exe))
Download the files from the mySqlFiles branch.

```bash
Paste my.ini into:
<InstalledDisk>:\xampp\mysql\bin\my.ini
Paste php.ini into:
<InstalledDisk>:\xampp\php\php.ini
```
After that use the phpMyAdmin UI to access the mySqlAdmin page

```launch XAMPP: Apache - > actions: Start. MySQL - > actions: Start, Admin```

On the left panel of the opened window -> New: create a new database using utf-8 encoding, name it: "appdb"
inside "appdb" click on import and import the "appdb.sql" db file.

## Usage
#### Server
Open ```server_launch.py ```
#### Client
Open ```App3.sln```, navigate to ```Client.cs``` and change the private IP address field to your local machine's IP address(usually the server should print out the correct local IP address, so try using that output first)

After that connect your device and start debugging(running the app).
##### Attention: if errors are shown while attempting to build the project, try to close and re-open visual studio. If that doesn't help check that you have the required SDKs installed.

#### Database
Launch XAMPP: ```MySQL - > actions: Start```

## Contributing
N\A

## License
[MIT](https://choosealicense.com/licenses/mit/)

MIT License

Copyright (c) 2019 Eitan Krimer / Maxim Trebuhovsky

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.